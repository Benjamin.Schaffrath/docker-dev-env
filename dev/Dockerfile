FROM ubuntu:bionic
MAINTAINER Benjamin Schaffrath <benjamin.schaffrath@jtl-software.com>
LABEL Usage="docker run -e "DOMAIN=your-domain.com" -d -p [HOST WWW PORT NUMBER]:80 -p [HOST DB PORT NUMBER]:3306 \
	-p [HOST REDIS PORT NUMBER]:6379 Version="1.0""
	
ENV TERM=linux
# Fix debconf warnings upon build
ARG DEBIAN_FRONTEND=noninteractive
ARG PHP_VERSION=7.2
ENV PHP_VERSION=$PHP_VERSION
ARG USE_ELASTICSEARCH=false
ENV USE_ELASTICSEARCH=$USE_ELASTICSEARCH
RUN apt-get update && apt-get upgrade -y
RUN apt-get update \
    && apt-get install -y --no-install-recommends gnupg \
    && echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu bionic main" > /etc/apt/sources.list.d/ondrej-php.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C \
    && apt-get update \
    && apt-get -y --no-install-recommends install nano apt-utils openjdk-8-jre-headless openjdk-8-jdk git wget ant zip unzip curl ca-certificates apache2 redis-server \
	php${PHP_VERSION} \
	php${PHP_VERSION}-bz2 \
	php${PHP_VERSION}-cgi \
	php${PHP_VERSION}-cli \
	php${PHP_VERSION}-common \
	php${PHP_VERSION}-curl \
	php${PHP_VERSION}-dev \
	php${PHP_VERSION}-enchant \
	php${PHP_VERSION}-fpm \
	php${PHP_VERSION}-gd \
	php${PHP_VERSION}-gmp \
	php${PHP_VERSION}-imagick \
	php${PHP_VERSION}-imap \
	php${PHP_VERSION}-interbase \
	php${PHP_VERSION}-intl \
	php${PHP_VERSION}-json \
	php${PHP_VERSION}-ldap \
	php${PHP_VERSION}-mbstring \
	php${PHP_VERSION}-mysql \
	php${PHP_VERSION}-odbc \
	php${PHP_VERSION}-opcache \
	php${PHP_VERSION}-pgsql \
	php${PHP_VERSION}-phpdbg \
	php${PHP_VERSION}-pspell \
	php${PHP_VERSION}-readline \
	php${PHP_VERSION}-recode \
	php${PHP_VERSION}-sqlite3 \
	php${PHP_VERSION}-sybase \
	php${PHP_VERSION}-tidy \
	php${PHP_VERSION}-xdebug \
	php${PHP_VERSION}-xmlrpc \
	php${PHP_VERSION}-xsl \
	php${PHP_VERSION}-xml \
	php${PHP_VERSION}-zip \
	&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* ~/.composer \
	&& a2enconf php${PHP_VERSION}-fpm \
    && a2enmod proxy \
    && a2enmod proxy_fcgi \
    && a2enmod rewrite
	
RUN apt-get update && apt-get install mariadb-common mariadb-server mariadb-client -y

RUN if [ "$USE_ELASTICSEARCH" != "false" ] ; then \
   wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add - \
   && echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" >> /etc/apt/sources.list.d/elastic.list \
   && apt update \
   && apt install elasticsearch kibana \
   && echo "server.host: \"0.0.0.0\"" >> /etc/kibana/kibana.yml \
   && echo "server.port: 5601" >> /etc/kibana/kibana.yml \
   && echo "transport.host: localhost" >> /etc/elasticsearch/elasticsearch.yml \
   && echo "transport.tcp.port: 9300 " >> /etc/elasticsearch/elasticsearch.yml \
   && echo "http.port: 9200" >> /etc/elasticsearch/elasticsearch.yml \
   && echo "network.host: 0.0.0.0" >> /etc/elasticsearch/elasticsearch.yml \
   && service kibana restart \
   && service elasticsearch start \
 ; fi

ENV LOG_STDERR true
ENV LOG_LEVEL debug
ENV ALLOW_OVERRIDE All
ENV DATE_TIMEZONE UTC
ENV TERM dumb

COPY xdebug.ini /etc/php/mods-available/
COPY run-lamp.sh /usr/sbin/

RUN chmod +x /usr/sbin/run-lamp.sh
RUN chmod +x /etc/php/mods-available/
RUN chown -Rf www-data:www-data /var/www/

EXPOSE 80
EXPOSE 3306
EXPOSE 6379
EXPOSE 5601
EXPOSE 9200

CMD ["/usr/sbin/run-lamp.sh"]
