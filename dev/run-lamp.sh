#!/bin/sh

MYSQL_CHARSET=${MYSQL_CHARSET:-"utf8"}
MYSQL_COLLATION=${MYSQL_COLLATION:-"utf8_unicode_ci"}
DB_NAME=${DB_NAME:-"database"}
DB_USER=${DB_USER:-"jtl"}
DB_PASS=${DB_PASS:-"jtlgmbh"}

echo "######Configuring Apache2 now!######"
if [ $ALLOW_OVERRIDE == 'All' ]; then
    /bin/sed -i 's/AllowOverride\ None/AllowOverride\ All/g' /etc/apache2/apache2.conf
fi

if [ $LOG_LEVEL != 'warn' ]; then
    /bin/sed -i "s/LogLevel\ warn/LogLevel\ ${LOG_LEVEL}/g" /etc/apache2/apache2.conf
fi

# Run MariaDB
echo "######Running and configuring MariaDB now!######"

/usr/bin/mysqld_safe --timezone=${DATE_TIMEZONE}&

# wait for mysql server to start (max 30 seconds)
timeout=30
#sudo chmod -R 777 /var/lib/mysql/
echo -n "Waiting for database server to accept connections"
while ! /usr/bin/mysqladmin -u root status >/dev/null 2>&1
do
  timeout=$(($timeout - 1))
  if [ $timeout -eq 0 ]; then
	echo -e "\nCould not connect to database server. Aborting..."
	exit 1
  fi
  echo -n "."
  sleep 1
done

mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS';"
mysql -uroot -e "CREATE DATABASE IF NOT EXISTS \`$DB_NAME\` DEFAULT CHARACTER SET \`$MYSQL_CHARSET\` COLLATE \`$MYSQL_COLLATION\`;"

#Configure PHPMYADMIN
echo "######Configuring PHPMYADMIN now!######"
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password jtlgmbh" | debconf-set-selections
echo "phpmyadmin phpmyadmin/db/app-user string jtl@localhost" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password jtlgmbh" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password jtlgmbh" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | debconf-set-selections

apt-get install -y phpmyadmin
sed -i "s/|\s*\((count(\$analyzed_sql_results\['select_expr'\]\)/| (\1)/g" /usr/share/phpmyadmin/libraries/sql.lib.php

echo "######Starting PHP-FPM now!######"
# Run PHP-FPM
echo "max_execution_time = 200" >> /etc/php/$PHP_VERSION/fpm/php.ini
echo "upload_max_filesize = 128M" >> /etc/php/$PHP_VERSION/fpm/php.ini
echo "post_max_size = 128M" >> /etc/php/$PHP_VERSION/fpm/php.ini
/etc/init.d/php$PHP_VERSION-fpm start

echo "######Configuring Redis now!######"

echo "requirepass ${REDIS_PW:-jtlgmbh}" >> /etc/redis/redis.conf

if [ $USE_ELASTICSEARCH ]; then
    service kibana restart
    service elasticsearch start
fi

/usr/sbin/apachectl -DFOREGROUND -k start -e debug

/bin/ln -sf /dev/stdout /var/log/apache2/access.log
