## Already installed stuff
* Apache2
* PHP (Can be choosen)
* Composer
* MariaDB
* PHPMyAdmin
* xDebug (IP configuration not needed by default)
* Redis
* Optional: ElasticSearch + Kibana (Not available at the moment)

## Inside the repository directory run
    docker build --build-arg PHP_VERSION=7.2 --tag "chakratos:devenv" dev/
## After that you can run the container from anywhere you want
    docker run -d \
      --name devenv \
      -p [HOST WWW PORT NUMBER]:80 \ 
      -p [HOST DB PORT NUMBER]:3306 \
      -p [HOST REDIS PORT NUMBER]:6379 \
      -e "REDIS_PW=RedisPassword" \ THIS IS OPTIONAL, DEFAULT IS jtlgmbh
      -v /path/to/your/website/dir:/var/www/html \
      -v dockersql:/var/lib/mysql \
      chakratos:lamp

### Example usage
    docker run -d \
          --name devenv \
          -p 8087:80 \
          -p 8088:3306 \
          -p 8089:6379 \
          -v /var/www/rss-viewer:/var/www/html \
          -v dockersql:/var/lib/mysql \
          chakratos:devenv
          
### Stopping the container
    docker stop devenv
### Your settings will be saved, to run it with the same settings you already run it with use:
    docker container start devenv
### To run the container with different settings stop the container and then use:
    docker container rm devenv
#### After that rerun the docker run command with your new settings
## SSH into the Environment
    docker exec -it -e COLUMNS=$COLUMNS -e LINES=$LINES -e TERM=$TERM devenv /bin/bash
## Apache VHOST for the container
In order to connect to the hosted website inside the container you need to setup an reverse proxy.
#### Example
    <VirtualHost *:80>
        ServerName example.de
        ServerAlias www.example.de
        ServerAdmin example@example.com

        DefaultType none
        RewriteEngine on
        AllowEncodedSlashes on
        RequestHeader set X-Forwarded-Proto "http"
        RewriteCond %{QUERY_STRING} transport=polling
        RewriteRule /(.*)$ http://127.0.0.1:[HOST WWW PORT NUMBER]/$1 [P]
        ProxyRequests off
        ProxyPreserveHost On
        ProxyPass "/" "http://127.0.0.1:[HOST WWW PORT NUMBER]/"
        ProxyPassReverse "/" "http://127.0.0.1:[HOST WWW PORT NUMBER]/"
    </VirtualHost>
(Ports needs to be filled in without "[]")

## Using ElasticSearch + Kibana
By default ElasticSearch and Kibana are disabled, in order to use them you need to 
build the image with an extra build-arg. 

You also need to add another volume and two additional ports to your docker run command.

#### Example build 
    docker build --build-arg PHP_VERSION=7.2 --build-arg USE_ELASTICSEARCH=true --tag "chakratos:devenv" dev/

#### Example run
    docker run -d \
          --name devenv \
          -p 8087:80 \
          -p 8088:3306 \
          -p 8089:6379 \
          -p 8090:5601 \
          -p 8091:9200 \
          -e "USE_ELASTICSEARCH=true" \
          -v /var/www/rss-viewer:/var/www/html \
          -v dockersql:/var/lib/mysql \
          -v dockerelastic:/usr/share/elasticsearch/data \
          chakratos:devenv

## Using MariaDB
By default a standard user will be created and additionally a standard database will be created too (If it does not already exists).

The user and the database are defined in the run-lamp.sh that gets executed after the container is started.

#### MariaDB Databases and users are *persistend*
To kill everything MariaDB related stop your container and run:

    docker volume rm dockersql

## xDebug
Configure the xdebug.ini inside the dev folder to your liking. It will be copied over to the Dev-Environment.
